Development Setup
=================

Revel-Server
------------

Run Revel-Server with Mix
_________________________

When developing Revel-Server, using mix to run the source is a much
better development environment.

When inside the Revel-Server directory, run Revel-Server with::

    $ mix run --no-halt

To run Revel-Server with an interactive Elixir REPL, use::

    $ iex -S mix
  
