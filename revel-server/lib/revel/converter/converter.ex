defmodule Revel.Converter do
  require Logger
  use GenServer

  @moduledoc """
  GenServer module to handle file conversion
  """

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: :converter)
  end

  def convert_file(file_id, format, bitrate) do
    GenServer.call(:converter, {:convert_file, {file_id, format, bitrate}})
  end

  def conversion_complete(file_id) do
    GenServer.call(:converter, {:conversion_complete, {file_id}})
  end

  def handle_call({:convert_file, {file_id, extension, bitrate}}, {pid, reference}, tmp_dir) do
    # TODO: add support for conversions to mutiple different bitrates
    # TODO: probably will suffix the filename with _bitrate or something

    Logger.debug("Conversion requested")
    [file] = Revel.Database.get_files_by_id(file_id)
    Logger.debug("Retrieving file entry from database")
    converted_path = Path.join([tmp_dir, file_id <> extension])

    # Check current conversion state of the requested file
    case :ets.lookup(:file_table, file_id) do
      [{_file_id, :converting, path, pids}] ->
        # file conversion is happening right now
        Logger.debug("File conversion already in process")
        :ets.insert(:file_table, {file_id, :converting, path, [pids | self()]})
        # Subscribe the requesting process to the coversion complete notify list
        # This process will now be notified when the file coversion is complete
        {:reply, :converting, tmp_dir}

      [{_file_id, :converted, path}] ->
        # file conversion has already been completed
        Logger.debug("File has already been converted")
        {:reply, {:converted, path}, tmp_dir}

      [] ->
        :ets.insert(:file_table, {file_id, :converting, converted_path, [pid]})
        # file has not been converted
        Logger.debug("Spawning conversion thread")

        # start conversion in another thread
        spawn(Revel.Converter, :do_conversion, [file, converted_path, bitrate])
        {:reply, :converting, tmp_dir}
    end
  end

  def handle_call({:conversion_complete, {file_id}}, _from, tmp_dir) do
    Logger.debug(":conversion_complete message recieved for file " <> file_id)
    # Conversion for file file_id is complete
    # Lookup entry from ETS to get path and pids
    [{_file_id, :converting, path, pids}] = :ets.lookup(:file_table, file_id)
    # Notify all subscribed PIDs that file conversion is complete
    Enum.map(pids, fn pid -> send(pid, {:conversion_complete, path}) end)
    :ets.insert(:file_table, {file_id, :converted, path})
    {:noreply, tmp_dir}
  end

  def do_conversion(file, out, bitrate) do
    Logger.debug("Converting " <> file[:filepath] <> " to " <> out)
    Logger.debug("Invoking vice in synchronous mode")
    # start conversion
    # compression: value is the desired bitrate
    # :sync argument tells vice to run in synchronous mode
    :vice.convert(file[:filepath], out, [compression: String.to_integer(bitrate)], :sync)
    Logger.debug("Conversion complete")
    Logger.debug("Sending :conversion_complete message to parent")
    # Tell Revel.Converter that the file conversion is complete
    Revel.Converter.conversion_complete(Integer.to_string(file[:id]))
  end

  def init(_constant) do
    # tmp_dir config resolution
    # If tmp_dir in specified in config.ini, that value is used
    # If not, $XDG_CACHE_HOME/revel is used is defined
    # If not defined, $HOME/.cache/revel is used

    tmp_dir_config = Revel.Config.get_val("Paths", "tmp_dir")

    tmp_dir =
      case tmp_dir_config do
        "" ->
          xdg_cache = System.get_env("XDG_CACHE_HOME")
          home = System.get_env("HOME")

          cond do
            xdg_cache != nil ->
              Logger.debug("Locating cache directory using $XDG_CACHE_HOME (" <> xdg_cache <> ")")
              tmp_dir = Path.join([xdg_cache, "revel"])
              tmp_dir

            home != nil ->
              Logger.debug("Locating cache directory using $HOME (" <> home <> ")")
              tmp_dir = Path.join([home, ".cache", "revel"])
              tmp_dir
          end

        _ ->
          tmp_dir_config
      end

    # create cache directory (will do nothing if it already exists
    File.mkdir_p(tmp_dir)
    # Create an ETS table to store file conversion status
    IO.inspect(:ets.new(:file_table, [:set, :protected, :named_table]))
    {:ok, tmp_dir}
  end
end
