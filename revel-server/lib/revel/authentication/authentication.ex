defmodule Revel.Authentication do
  require Logger
  import Revel.Common

  def init(opts), do: opts

  def encoded_pass_check(given_password, db_password) do
    [_, cleaned_password] = String.split(given_password, ":")

    given_password_string =
      cleaned_password
      |> String.graphemes()
      |> Enum.chunk_every(2)
      |> Enum.map(fn [a, b] -> Integer.parse(a <> b, 16) end)
      |> Enum.map(fn {a, _} -> a end)

    String.equivalent?(given_password_string, db_password)
  end

  def plaintext_pass_check(given_password, db_password) do
    String.equivalent?(given_password, db_password)
  end

  def user_pass_auth(conn) do
    given_username = conn.query_params["u"]
    given_password = conn.query_params["p"]

    query = Revel.Database.get_user(given_username)

    case query do
      [] ->
        {false, 40}

      [
        [
          username: _db_username,
          password: db_password,
          admin_role: admin_role,
          settings_role: _settings_role,
          download_role: _download_role,
          playlist_role: _playlist_role,
          cover_art_role: _cover_art_role
        ]
      ] ->
        authenticated =
          case String.starts_with?(given_password, "enc:") do
            true ->
              encoded_pass_check(given_password, db_password)

            false ->
              plaintext_pass_check(given_password, db_password)
          end

        case authenticated do
          true ->
            {true, admin_role}

          false ->
            {false, 40}
        end
    end
  end

  def user_token_salt_auth(conn) do
    given_username = conn.query_params["u"]
    given_salt = conn.query_params["s"]
    given_token = conn.query_params["t"]

    query = Revel.Database.get_user(given_username)

    case query do
      [] ->
        {false, 40}

      [
        [
          username: _db_username,
          password: db_password,
          admin_role: admin_role,
          settings_role: _settings_role,
          download_role: _download_role,
          playlist_role: _playlist_role,
          cover_art_role: _covert_art_role
        ]
      ] ->
        db_token = password_salt_hash(db_password, given_salt)

        case String.equivalent?(given_token, db_token) do
          true ->
            {true, admin_role}

          false ->
            {false, 40}
        end
    end
  end

  def authenticated(conn) do
    conn = Plug.Conn.fetch_query_params(conn)
    user_pass_auth_required_keys = ["u", "p"]
    user_token_salt_auth_required_keys = ["u", "t", "s"]

    cond do
      check_map_for_keys(conn.query_params, user_pass_auth_required_keys) == true ->
        user_pass_auth(conn)

      check_map_for_keys(conn.query_params, user_token_salt_auth_required_keys) == true ->
        user_token_salt_auth(conn)

      true ->
        {false, 0}
    end
  end

  def call(conn, _opts) do
    Logger.debug("Authentication request from " <> to_string(:inet_parse.ntoa(conn.remote_ip)))

    case authenticated(conn) do
      {true, admin_role} ->
        new_assigns =
          conn.assigns
          |> Map.put(:authenticated, true)
          |> Map.put(:admin, !!admin_role)

        %{conn | assigns: new_assigns}

      {false, error_code} ->
        new_assigns =
          conn.assigns
          |> Map.put(:authenticated, false)
          |> Map.put(:admin, false)

        conn = %{conn | assigns: new_assigns}
        send_error(conn, error_code)
    end
  end

  @doc "Converted plaintex password and salt into salted_hash in base16 lowercase"
  defp password_salt_hash(password, salt) do
    :crypto.hash(:md5, password <> salt) |> Base.encode16(case: :lower)
  end
end
