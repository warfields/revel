defmodule Revel.Router do
  require Logger
  use Plug.Router

  import Revel.Common

  # plug(Revel.Authentication)
  plug(:match)
  plug(:dispatch)

  @moduledoc """
  Top level router.
  Determines order of modules called
  (ex: Revel.Authentication before Revel.RestRouter)
  """

  def child_spec(_opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, []},
      type: :worker,
      restart: :permanent,
      shutdown: 500
    }
  end

  def start_link() do
    # Custom start_link function allows specifying ip and port at runtime
    ip_string = Revel.Config.get_val("Network", "listen_ip")

    {:ok, ip_tuple} =
      ip_string
      |> to_charlist
      |> :inet.parse_address()

    port_string = Revel.Config.get_val("Network", "listen_port")
    {port_int, _} = port_string |> Integer.parse()

    Logger.info("Starting revel-server on " <> ip_string <> ":" <> port_string)
    {:ok, ret} = Plug.Adapters.Cowboy.http(Revel.Router, [], ip: ip_tuple, port: port_int)
    Logger.info("revel-server started sucessfully")

    {:ok, ret}
  end

  def call(%Plug.Conn{request_path: path} = conn, _opts) do
    conn = Plug.Conn.fetch_query_params(conn)

    # Check if user is authenticated first using Revel.Authentication
    conn = Revel.Authentication.call(conn, Revel.Authentication.init([]))

    # Check if the client wants a json or xml response
    conn =
      case check_map_for_keys(conn.query_params, ["f"]) do
        true ->
          case conn.query_params["f"] do
            "xml" ->
              %{
                conn
                | assigns:
                    conn.assigns
                    |> Map.put(:format, :xml)
              }

            "json" ->
              %{
                conn
                | assigns:
                    conn.assigns
                    |> Map.put(:format, :json)
              }
          end

        false ->
          %{
            conn
            | assigns:
                conn.assigns
                |> Map.put(:format, :xml)
          }
      end

    case conn.assigns.authenticated do
      true ->
        Logger.debug("User authenticated")

        prefix_path = Revel.Config.get_val("Network", "path")
        # Make sure prefix_path ends with a /
        prefix_path =
          case String.ends_with?(prefix_path, "/") do
            true -> prefix_path
            false -> prefix_path <> "/"
          end

        # Create regex looking for /prefix_path/rest/.+
        regex_string = prefix_path <> "rest/" <> "(.+)"
        {:ok, regex} = Regex.compile(regex_string)
        # Run regex, storing capture group in new_route
        case Regex.run(regex, path) do
          [_, new_route] ->
            Logger.debug("Request made to endpoint: " <> new_route)
            # Update request_path and path_info keys in conn to new route
            conn = %{conn | request_path: new_route, path_info: [new_route]}
            # Forward route on the Revel.RestRouter
            Revel.RestRouter.call(conn, Revel.RestRouter.init([]))

          nil ->
            send_resp(conn, 404, "Not Found")
        end

      false ->
        Logger.info("User failed to authenticate")
        conn
    end
  end

  match _ do
    send_resp(conn, 404, "Not Found")
  end
end
