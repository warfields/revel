defmodule Revel.Scanner do
  require Logger
  use Task

  def start_link({parent, database}) do
    Task.start_link(__MODULE__, :run, [database, parent])
  end

  def run(database, parent) do
    case run_indexer(database) do
      {:ok, 0} ->
        send(parent, :scan_complete)

      {:error, _return_code} ->
        send(parent, :scan_failed)
    end
  end

  def run_indexer(database) do
    Logger.debug(
      "Invoking revel indexer (revel-indexer " <>
        database <> " " <> Revel.Config.get_val("Paths", "music_path") <> ")"
    )

    case System.cmd("revel-indexer", [database, Revel.Config.get_val("Paths", "music_path")]) do
      {_stdout, 0} ->
        {:ok, 0}

      {stdout, return_code} ->
        Logger.error(
          "revel-indexer failed with return code " <> return_code <> " and output " <> stdout
        )

        {:error, return_code}
    end
  end
end
