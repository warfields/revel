defmodule Revel.Common do
  require Logger
  import Plug.Conn
  import XmlBuilder
  import FFmpex
  use FFmpex.Options

  @moduledoc """
  Revel.Common is mostly convenience functions to make
  revel more readable and maintainable
  """

  @doc "Transcodes a file to the requested bitrate/format and sends it"
  def send_transcoded_file(conn) do
    file_id = conn.query_params["id"]
    max_bitrate = conn.query_params["maxBitRate"]
    extension = ".mp3"

    output_file =
      case Revel.Converter.convert_file(file_id, extension, max_bitrate) do
        {:converted, path} ->
          path

        :converting ->
          receive do
            {:conversion_complete, output_file} ->
              output_file
          end
      end

    conn
    |> put_resp_content_type("audio/mpeg")
    |> send_file(200, output_file)
  end

  @doc "Sends the requested file straight from disk"
  def send_file(conn) do
    [file] = Revel.Database.get_files_by_id(conn.query_params["id"])
    mime_type = MIME.type(file[:ext])

    conn
    |> put_resp_content_type(mime_type)
    |> send_file(200, file[:filepath])
  end

  @doc "Checks a map for given keys. Useful for checking contents of conn.query_params"
  def check_map_for_keys(map, required_keys) do
    required_keys |> Enum.all?(&Map.has_key?(map, &1))
  end

  @doc """
  Checks the map for the given key. If the key exists converts it to a bool and returns it.
  If the key does not exist, return the given default value
  """
  def convert_bool_literal_from_map_to_int(map, key, default) do
    case check_map_for_keys(map, [key]) do
      true ->
        case map[key] do
          "True" ->
            1

          "False" ->
            0
        end

      false ->
        default
    end
  end

  @doc "Sends the given xml as mime type text/xml;charset=UTF-8"
  def send_xml(conn, xml) do
    conn
    |> put_resp_content_type("text/xml;charset=UTF-8")
    |> send_resp(200, xml)
  end

  @doc "Sends the given json as mime type application/json"
  def send_json(conn, json) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, json)
  end

  @doc "Send json or xml containing the specified error"
  def send_error(conn, error_code) do
    Logger.error(
      to_string(:inet_parse.ntoa(conn.remote_ip)) <>
        ": Error " <> to_string(error_code) <> ": " <> error_string(error_code)
    )

    case conn.assigns.format do
      :xml ->
        send_error_xml(conn, error_code)

      :json ->
        send_error_json(conn, error_code)
    end
  end

  @doc "Sends json containg the specified error"
  def send_error_json(conn, error_code) do
    resp =
      Jason.encode!(%{
        "subsonic-response" => %{
          "status" => "failed",
          "version" => "1.15.0",
          "error" => %{"code" => error_code, "message" => error_string(error_code)}
        }
      })

    send_json(200, resp)
  end

  @doc "Sends xml containing the specified error"
  def send_error_xml(conn, error_code) do
    resp =
      element(
        :"subsonic-response",
        %{
          xmlns: "http://subsonic.org/restapi",
          status: "failed",
          version: "1.15.0"
        },
        [
          element(:error, %{
            code: error_code,
            message: error_string(error_code)
          })
        ]
      )

    xml_resp = resp |> XmlBuilder.generate()

    send_xml(conn, xml_resp)
  end

  @doc "Returns the error string associated with the given error code"
  def error_string(error_code) do
    case error_code do
      0 -> "A generic error"
      10 -> "Required parameter is missing"
      20 -> "Incompatible Subsonic REST protocol version. Client must upgrade."
      30 -> "Incompatible Subsonic REST protocol version. Server must upgrade."
      40 -> "Wrong username or password."
      50 -> "User is not authorized for the given operation."
      70 -> "The requested data was not found."
      _ -> ""
    end
  end
end
